package it.kdde.sequentialpatterns.model;

import java.io.Serializable;

/**
 * Created by fabiofumarola on 15/11/14.
 */
public class VerticalIdList implements Serializable{

    private ListNode[] elements;
    private long absoluteSupport;

    public VerticalIdList(ListNode[] elements, long absoluteSupport){
        this.elements = elements;
        this.absoluteSupport = absoluteSupport;
    }

    public ListNode[] getElements() {
        return elements;
    }
    
    public long getAbsoluteSupport(){return absoluteSupport;}

    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();

        for(ListNode el:elements){
            if(el!=null)
                buf.append(el.toString());
            
        }

        return buf.toString();
    }
}
