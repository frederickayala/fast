package it.kdde.sequentialpatterns.apriori;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by fabiofumarola on 20/11/14.
 */
public class AprioriDatasetTest {

    @Test
    public void loadFromPrefixSpanSource() throws IOException {
        Path path = Paths.get("src/test/resources/contextPrefixSpan.txt");
        AprioriDataset aprioriDataset = AprioriDataset.fromPrefixSpanSource(path, 0.2F);

        assert aprioriDataset.size() > 0;
    }
}
